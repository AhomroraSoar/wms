import React, { useEffect, useState } from "react";
import Swal from "sweetalert2";
import { motion } from "framer-motion";

// MUI import
import { createTheme, ThemeProvider } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Avatar from "@mui/material/Avatar";
import Stack from "@mui/material/Stack";

//Icon Stuff
import ShowChartOutlinedIcon from "@mui/icons-material/ShowChartOutlined";

//import component
import Appbar from "../appbar";
import Chart from "./Chart";
import Product from "./Product";
import Stock from "./Stock";
import Exported from "./Exported";
import Remaining from "./Remaining";

const mdTheme = createTheme({
  palette: {
    primary: {
      main: "#001345",
      info: "#e7e8fc",
    },
    secondary: {
      main: "#878787",
    },
  },
});

function DashboardContent() {
  const [anchorElNav, setAnchorElNav] = React.useState(null);
  const [anchorElUser, setAnchorElUser] = React.useState(null);

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
    >
      <ThemeProvider theme={mdTheme}>
        <CssBaseline />
        <Grid container spacing={18} sx={{ border: 0 }}>
          <Grid item xs={12}>
            <Box
              sx={{
                bgcolor: "primary.info",
                width: "auto",
                height: "100%",
                borderLeft: 71,
                borderColor: "primary.main",
              }}
            >
              <Appbar />
              <Container maxWidth="xlg" sx={{ pt: 9, mt: 5, mb: 4, border: 0 }}>
                <Grid container>
                  {/* Fluid grids */}
                  <Grid item xs={3} sx={{ border: 0 }}>
                    <Box
                      sx={{
                        pl: 2,
                        alignItems: "center",
                        display: "flex",
                        flexDirection: "column",
                      }}
                    >
                      {/* Tags Counted */}
                      <Paper
                        sx={{
                          p: 2,
                          mb: 2,
                          display: "flex",
                          borderRadius: "11px",
                          flexDirection: "column",
                          height: 200,
                          width: 286,
                        }}
                      >
                        <Typography
                          sx={{
                            justifyContent: "flex-start",
                            fontSize: "20px",
                            pb: 3,
                          }}
                        >
                          จำนวนสินค้าที่ติดแท็กแล้ว
                        </Typography>
                        <Product />
                      </Paper>

                      {/* Tags Counted */}

                      <Paper
                        sx={{
                          p: 2,
                          mb: 2,
                          display: "flex",
                          borderRadius: "11px",
                          flexDirection: "column",
                          height: 140,
                          width: 286,
                        }}
                      >
                        <Typography
                          sx={{
                            justifyContent: "flex-start",
                            fontSize: "18px",
                          }}
                        >
                          Today's products in stock
                        </Typography>
                        <Box sx={{ alignItems: "center" }}>
                          <Stock />
                        </Box>
                      </Paper>

                      {/* Tags Counted */}

                      <Paper
                        sx={{
                          p: 2,
                          mb: 2,
                          display: "flex",
                          borderRadius: "11px",
                          flexDirection: "column",
                          height: 140,
                          width: 286,
                        }}
                      >
                        <Typography
                          sx={{
                            justifyContent: "flex-start",
                            fontSize: "18px",
                          }}
                        >
                          Exported
                        </Typography>
                        <Box
                          sx={{
                            mt: 1.75,
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                          }}
                        >
                          <Typography
                            component="p"
                            variant="h6"
                            sx={{ textAlign: "center", fontSize: "38px" }}
                          >
                            0
                          </Typography>
                        </Box>
                      </Paper>

                      {/* Tags Counted */}

                      <Paper
                        sx={{
                          p: 2,
                          mb: 4.25,
                          display: "flex",
                          borderRadius: "11px",
                          flexDirection: "column",
                          height: 140,
                          width: 286,
                        }}
                      >
                        <Typography
                          sx={{
                            justifyContent: "flex-start",
                            fontSize: "18px",
                          }}
                        >
                          Remaining
                        </Typography>
                        <Box sx={{ alignItems: "center" }}>
                          <Stock />
                        </Box>
                      </Paper>

                      <Paper
                        sx={{
                          p: 2,
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                          borderRadius: "11px",
                          flexDirection: "column",
                          height: 150,
                          width: 292,
                        }}
                      >
                        <Typography
                          sx={{
                            justifyContent: "flex-start",
                            fontSize: "16px",
                          }}
                        >
                          <ShowChartOutlinedIcon
                            sx={{ color: "#DB5116", mr: 0.5 }}
                          />
                          Stock
                        </Typography>
                        <Typography
                          sx={{
                            justifyContent: "flex-start",
                            fontSize: "16px",
                            pt: 1.25,
                            pb: 1.25,
                          }}
                        >
                          <ShowChartOutlinedIcon
                            sx={{ color: "#0A3000", mr: 0.5 }}
                          />
                          Exported
                        </Typography>
                        <Typography
                          sx={{
                            justifyContent: "flex-start",
                            fontSize: "16px",
                          }}
                        >
                          <ShowChartOutlinedIcon
                            sx={{ color: "#000CF9", mr: 0.5 }}
                          />
                          Remaining
                        </Typography>
                      </Paper>
                    </Box>
                  </Grid>

                  {/* Fluid grids */}
                  <Grid container item xs={9} sx={{ display: "flex" }}>
                    {/* Chart */}

                    <Paper
                      sx={{
                        p: 0,
                        pl: 4,
                        mr: 5.5,
                        display: "flex",
                        flexDirection: "column",
                        width: 500,
                        height: 300,
                        borderRadius: "11px",
                        justifyContent: "center",
                      }}
                    >
                      <Exported />
                    </Paper>

                    <Paper
                      sx={{
                        p: 0,
                        pl: 4,
                        mr: 5.5,
                        display: "flex",
                        flexDirection: "column",
                        width: 250,
                        height: 300,
                        borderRadius: "11px",
                        justifyContent: "center",
                      }}
                    >
                      <Stack direction="row" spacing={2}>
                        <Paper
                          sx={{
                            height: 20,
                            width: 20,
                            bgcolor: "#ddfafb",
                            mb: 2,
                          }}
                        ></Paper>
                        <Typography> T-Shirt </Typography>
                      </Stack>

                      <Stack direction="row" spacing={2}>
                        <Paper
                          sx={{
                            height: 20,
                            width: 20,
                            bgcolor: "#ea8bcb",
                            mb: 2,
                          }}
                        ></Paper>
                        <Typography> Polo Shirt </Typography>
                      </Stack>

                      <Stack direction="row" spacing={2}>
                        <Paper
                          sx={{
                            height: 20,
                            width: 20,
                            bgcolor: "#fff59d",
                            mb: 2,
                          }}
                        ></Paper>
                        <Typography> Cardigan </Typography>
                      </Stack>

                      <Stack direction="row" spacing={2}>
                        <Paper
                          sx={{
                            height: 20,
                            width: 20,
                            bgcolor: "#001345",
                            mb: 2,
                          }}
                        ></Paper>
                        <Typography> Jeans </Typography>
                      </Stack>

                      <Stack direction="row" spacing={2}>
                        <Paper
                          sx={{ height: 20, width: 20, bgcolor: "#c5a0ff" }}
                        ></Paper>
                        <Typography> Skirt </Typography>
                      </Stack>
                    </Paper>

                    <Paper
                      sx={{
                        p: 2,
                        display: "flex",
                        flexDirection: "column",
                        width: 457,
                        height: 300,
                        borderRadius: "11px",
                        justifyContent: "center",
                      }}
                    >
                      <Remaining />
                    </Paper>

                    {/* Recent Orders */}
                    <Paper
                      sx={{
                        mt: 5,
                        p: 2,
                        display: "flex",
                        flexDirection: "column",
                        borderRadius: "11px",
                        width: 1300,
                        height: 500,
                      }}
                    >
                      <Chart />
                    </Paper>
                  </Grid>
                </Grid>
              </Container>
            </Box>
          </Grid>
        </Grid>
      </ThemeProvider>
    </motion.div>
  );
}

export default function Dashboard() {
  return <DashboardContent />;
}
