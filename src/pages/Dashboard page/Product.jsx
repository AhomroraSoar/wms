import React, { useEffect, useState } from "react";
import Typography from "@mui/material/Typography";

function preventDefault(event) {
  event.preventDefault();
}

export default function Product() {
  const [tag, setTag] = useState([]);
  useEffect(() => {
    tagGet();
  }, []);

  const tagGet = () => {
    fetch("https://crud-wms.herokuapp.com/connectedTags")
      .then((res) => res.json())
      .then((rows) => {
        setTag(rows);
      });
  };

  return (
    <React.Fragment>
      <Typography
        color="primary"
        component="p"
        variant="h3"
        sx={{ textAlign: "center", pt: 1.5 }}
      >
        {tag.length}
      </Typography>
    </React.Fragment>
  );
}
