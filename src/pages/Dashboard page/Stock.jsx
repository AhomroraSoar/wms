import React, { useEffect, useState } from "react";
import Typography from "@mui/material/Typography";

function preventDefault(event) {
  event.preventDefault();
}

export default function Stock() {
  const [quan, setQuan] = useState([]);
  useEffect(() => {
    quanGet();
  }, []);

  const quanGet = () => {
    fetch("https://crud-wms.herokuapp.com/sumQuan")
      .then((res) => res.json())
      .then((rows) => {
        setQuan(rows);
      });
  };

  return (
    <React.Fragment>
      <Typography
        component="p"
        sx={{
          textAlign: "center",
          fontSize: "38px",
          justifyContent: "center",
          alignItems: "center",
          mt: 1.75,
        }}
      >
        {quan.TotalQuantity}
      </Typography>
    </React.Fragment>
  );
}
