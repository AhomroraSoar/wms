import React, { useEffect, useState } from "react";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import { CardActionArea } from "@mui/material";
import { styled } from "@mui/material/styles";
import Collapse from "@mui/material/Collapse";
import IconButton from "@mui/material/IconButton";
import CardActions from "@mui/material/CardActions";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";

const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? "rotate(0deg)" : "rotate(180deg)",
  marginLeft: "auto",
  transition: theme.transitions.create("transform", {
    duration: theme.transitions.duration.shortest,
  }),
}));

export default function homecard() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    productsGet();
  }, []);

  const productsGet = () => {
    fetch("https://crud-wms.herokuapp.com/pd")
      .then((res) => res.json())
      .then((rows) => {
        setProducts(rows);
      });
  };

  const [expandedId, setExpandedId] = React.useState(-1);

  const handleExpandClick = (i) => {
    setExpandedId(expandedId === i ? -1 : i);
  };

  return (
    <Grid container>
      {products.slice(0, 12).map((products, i) => (
        <Grid item key={products.product_id} display="flex" xs={3}>
          <Card
            raised="true"
            sx={{
              width: 400,
              p: 3,
              m: 3,
              raised: true,
            }}
            style={{
              borderRadius: 20,
            }}
          >
            <CardActionArea href="/Products" sx={{ maxHeight: 250 }}>
              <CardMedia
                component="img"
                sx={{
                  height: 250,
                  width: 340,
                  display: "flex",
                  justifyContent: "center",
                }}
                image={products.product_picture}
              />
            </CardActionArea>
            <CardActions disableSpacing={true}>
              <Typography variant="h5" style={{ fontSize: 20 }}>
                {products.product_name}
              </Typography>
              <ExpandMore
                expand={expandedId === i}
                onClick={() => handleExpandClick(i)}
                aria-expanded={expandedId === i}
              >
                <ExpandMoreIcon />
              </ExpandMore>
            </CardActions>
            <Collapse in={expandedId === i} timeout="auto">
              <CardContent>
                <Typography paragraph>{products.description}</Typography>
              </CardContent>
            </Collapse>
          </Card>
        </Grid>
      ))}
    </Grid>
  );
}
