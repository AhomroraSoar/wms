import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import CssBaseline from "@mui/material/CssBaseline";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Avatar from "@mui/material/Avatar";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";

import { createTheme, ThemeProvider } from "@mui/material/styles";

import Swal from "sweetalert2";

import BorderColorIcon from "@mui/icons-material/BorderColor";

export default function UserUpdate() {
  const { id } = useParams();
  useEffect(() => {
    fetch("https://crud-wms.herokuapp.com/Multags/" + id)
      .then((res) => res.json())
      .then((result) => {
        let tag = result.map((a) => a.tag_id);
        let tag_detail = result.map((a) => a.tag_detail);
        setTag_id(tag);
        setTag_detail(tag_detail);
      });
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();
    var data = {
      tag_id: id,
      tag_detail: tag_detail,
      product_id: product_id,
    };
    fetch("https://crud-wms.herokuapp.com/updateMultag", {
      method: "PUT",
      headers: {
        Accept: "application/form-data",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((result) => {
        const ok = JSON.stringify(result.status);
        if (ok == ok) {
          Swal.fire({
            icon: "success",
            title: "Updated!",
            text: result["message"],
            timer: 2000,
            timerProgressBar: true,
            showConfirmButton: false,
          });
          window.setTimeout(function () {
            window.location.href = "/Tag";
          }, 2000);
        }
      })

      .catch((error) => console.log("error", error));
  };

  const [tag_id, setTag_id] = useState("");
  var valueID = "";
  valueID = valueID + "" + tag_id + "";
  const [tag_detail, setTag_detail] = useState("");
  var valueDetail = "";
  valueDetail = valueDetail + "" + tag_detail + "";
  const [product_id, setProduct_id] = useState("");
  const [products, setProduct] = useState([]);

  const ProductGet = () => {
    fetch("https://crud-wms.herokuapp.com/pd")
      .then((res) => res.json())
      .then((rows) => {
        setProduct(rows);
      });
  };
  useEffect(() => {
    ProductGet();
  }, []);
  const theme = createTheme({
    palette: {
      primary: {
        main: "#142454",
      },
    },
  });

  const avatarStyle = { backgroundColor: "#041444" };

  return (
    <ThemeProvider theme={theme}>
      <Grid container component="main" sx={{ height: "100vh" }}>
        <CssBaseline />
        <Grid
          item
          xs={false}
          sm={4}
          md={7}
          sx={{
            backgroundImage:
              "url(https://cdn.discordapp.com/attachments/744696824220221502/984030671427231775/unknown.png)",
            backgroundRepeat: "no-repeat",
            backgroundColor: (t) =>
              t.palette.mode === "light"
                ? t.palette.grey[50]
                : t.palette.grey[900],
            backgroundSize: "cover",
            backgroundPosition: "center",
          }}
        />
        <Grid
          item
          align="center"
          xs={12}
          sm={8}
          md={5}
          component={Paper}
          elevation={6}
          square
        >
          <Box
            sx={{
              my: 0,
              mx: 4,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              marginTop: 43,
              border: 0,
              height: 750,
            }}
          >
            <Avatar style={avatarStyle} sx={{ height: 85, width: 85 }}>
              <BorderColorIcon sx={{ height: 50, width: 50 }} />
            </Avatar>
            <Typography component="h1" variant="h5" sx={{ mt: 3 }}>
              Link Tag to Product
            </Typography>

            <form onSubmit={handleSubmit}>
              <Grid
                display="flex"
                container
                sx={{ mt: 1, mb: 1, border: 0 }}
                spacing={2}
              >
                <Grid
                  item
                  justifyContent="center"
                  alignItems="center"
                  xs={12}
                  sx={{ border: 0, mb: 1, pl: 2 }}
                >
                  <Typography
                    align="left"
                    noWrap="true"
                    sx={{
                      border: 1,
                      mx: 15.5,
                      borderRadius: 0.75,
                      py: 1.5,
                      borderColor: "#c4c4c4",
                      pl: 1.5,
                      maxWidth: 600,
                    }}
                  >
                    Tag Number : {valueID}
                  </Typography>
                </Grid>

                <Grid
                  item
                  justifyContent="center"
                  alignItems="center"
                  xs={12}
                  sx={{ border: 0, mb: 1, pl: 2 }}
                >
                  <Typography
                    align="left"
                    noWrap="true"
                    sx={{
                      border: 1,
                      mx: 15.5,
                      borderRadius: 0.75,
                      py: 1.5,
                      borderColor: "#c4c4c4",
                      pl: 1.5,
                      maxWidth: 600,
                    }}
                  >
                    Tag ID : {valueDetail}
                  </Typography>
                </Grid>

                <Grid
                  item
                  justifyContent="center"
                  xs={12}
                  sx={{ mb: 1, pl: 2 }}
                >
                  <FormControl style={{ width: 600 }}>
                    <InputLabel id="demo-simple-select-label">
                      Please select a product
                    </InputLabel>

                    <Select
                      variant="outlined"
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      label="Please select a product"
                      onChange={(e) => setProduct_id(e.target.value)}
                    >
                      <MenuItem value={null}>RESET</MenuItem>
                      {products.map((products) => (
                        <MenuItem
                          key={products.product_id}
                          value={products.product_id}
                        >
                          {products.product_name}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </Grid>

                <Grid
                  item
                  xs={12}
                  sx={{ display: "flex", justifyContent: "center" }}
                >
                  <Button
                    type="submit"
                    variant="contained"
                    sx={{ width: 150, height: 50 }}
                  >
                    Submit
                  </Button>
                </Grid>
              </Grid>
            </form>
          </Box>
        </Grid>
      </Grid>
    </ThemeProvider>
  );
}
