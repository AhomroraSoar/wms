import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";

// MUI import
import {
  alpha,
  styled,
  createTheme,
  useTheme,
  ThemeProvider,
} from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import TextField from "@mui/material/TextField";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableFooter from "@mui/material/TableFooter";
import TablePagination from "@mui/material/TablePagination";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Checkbox from "@mui/material/Checkbox";
import Toolbar from "@mui/material/Toolbar";
import Stack from "@mui/material/Stack";
import Avatar from "@mui/material/Avatar";

import Swal from "sweetalert2";

//Icon Stuff
import ClearIcon from "@mui/icons-material/Clear";
import SearchIcon from "@mui/icons-material/Search";
import FirstPageIcon from "@mui/icons-material/FirstPage";
import KeyboardArrowLeft from "@mui/icons-material/KeyboardArrowLeft";
import KeyboardArrowRight from "@mui/icons-material/KeyboardArrowRight";
import LastPageIcon from "@mui/icons-material/LastPage";
import DeleteForeverRoundedIcon from "@mui/icons-material/DeleteForeverRounded";
import ModeEditIcon from "@mui/icons-material/ModeEdit";

//import component
import Appbar from "../appbar";

function TablePaginationActions(props) {
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (event) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box sx={{ flexShrink: 0, ml: 2.5, border: 0 }}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowRight />
        ) : (
          <KeyboardArrowLeft />
        )}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowLeft />
        ) : (
          <KeyboardArrowRight />
        )}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </Box>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    fontSize: 20,
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 16,
    color: mdTheme.palette.primary.main,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}));

function EnhancedTableHead(props) {
  const { onSelectAllClick, numSelected, rowCount } = props;

  return (
    <TableHead>
      <StyledTableRow>
        <StyledTableCell align="center" width="5%">
          <Checkbox
            style={{
              color: "#FFFFFF",
            }}
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={rowCount > 0 && numSelected === rowCount}
            onChange={onSelectAllClick}
          />
        </StyledTableCell>

        <StyledTableCell align="center" width="5%">
          ID
        </StyledTableCell>
        <StyledTableCell align="left" width="20%">
          Tag's uniqueID
        </StyledTableCell>
        <StyledTableCell align="center" width="10%">
          Product's name
        </StyledTableCell>
        <StyledTableCell align="center" width="5%">
          Product's image
        </StyledTableCell>
        <StyledTableCell align="center" width="15%">
          Time stamp
        </StyledTableCell>
        <StyledTableCell align="center" width="10%">
          Action
        </StyledTableCell>
      </StyledTableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const mdTheme = createTheme({
  palette: {
    primary: {
      main: "#001345",
      info: "#e7e8fc",
    },
    secondary: {
      main: "#878787",
    },
  },
});

export default function Tagging() {
  const [platform, setPlatform] = useState([]);
  const [searchText, setSearchText] = useState("");
  const [rows] = useState([]);
  const [tag, setTag] = useState([]);
  const [registag, setRegistag] = useState([]);
  useEffect(() => {
    TagGet();
    registagGet();
  }, []);

  const TagGet = () => {
    fetch("https://crud-wms.herokuapp.com/tags")
      .then((res) => res.json())
      .then((rows) => {
        setTag(rows);
        setPlatform(rows);
      });
  };

  const registagGet = () => {
    fetch("https://crud-wms.herokuapp.com/connectedTags")
      .then((res) => res.json())
      .then((rows) => {
        setRegistag(rows);
      });
  };
  const UpdateTag = (tag_id) => {
    window.location = "/Updatetag/" + tag_id;
  };
  const UpdateMultag = (selected) => {
    window.location = "/UpdatemultipleTags/" + selected;
  };

  const TagDelete = (tag_id) => {
    var data = {
      tag_id: tag_id,
    };
    fetch("https://crud-wms.herokuapp.com/deletetag", {
      method: "DELETE",
      headers: {
        Accept: "application/form-data",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then((rows) => {
        if (rows["status"] === "ok") {
          Swal.fire({
            icon: "success",
            title: "Deleted!",
            text: rows["message"],
            toast: true,
            position: "top-end",
            showConfirmButton: false,
            timer: 2500,
            timerProgressBar: true,
          });
          TagGet();
          window.location = "/Tag";
        }
      });
  };

  function escapeRegExp(value) {
    return value.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
  }

  const requestSearch = (searchValue) => {
    const searchRegex = new RegExp(escapeRegExp(searchValue), "i");
    const filteredTag = platform.filter((tag) => {
      return Object.keys(tag).some((field) => {
        return searchRegex.test(tag[field]);
      });
    });
    setTag(filteredTag);
  };

  const [selected, setSelected] = React.useState([]);

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = tag.map((n) => n.tag_id);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, tag_id) => {
    const selectedIndex = selected.indexOf(tag_id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, tag_id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }

    setSelected(newSelected);
  };

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const isSelected = (tag_id) => selected.indexOf(tag_id) !== -1;

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const [file, setFile] = useState("");

  const saveFile = (e) => {
    setFile(e.target.files[0]);
  };

  const addCSV = async () => {
    console.warn(file);
    const formData = new FormData();
    formData.append("file", file);
    await fetch("https://crud-wms.herokuapp.com/tags/file", {
      method: "POST",
      body: formData,
    }).then((formData) => {
      console.log(formData);
      if (formData["statusText"] === "OK") {
        Swal.fire({
          icon: "success",
          title: "Uploaded!",
          text: "CSV File hes been uploaded successfully to our database!",
          toast: true,
          position: "bottom-end",
          showConfirmButton: false,
          timer: 2500,
          timerProgressBar: true,
        });
      }
    });
  };

  const deleteTagsbyids = async () => {
    console.log(selected);
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    await fetch("https://crud-wms.herokuapp.com/deleteMultag", {
      headers: myHeaders,
      method: "DELETE",
      body: JSON.stringify(selected),
    }).then((selected) => {
      console.log(selected);
      if (selected["statusText"] === "OK") {
        Swal.fire({
          icon: "success",
          title: "Deleted!",
          text: "Tags data has been deleted!",
          toast: true,
          position: "bottom-end",
          showConfirmButton: false,
          timer: 2500,
          timerProgressBar: true,
        });
      }
    });
  };

  const EnhancedTableToolbar = (props) => {
    const { numSelected } = props;

    return (
      <Toolbar
        sx={{
          pl: { sm: 2 },
          pr: { xs: 1, sm: 1 },
          ...(numSelected > 0 && {
            bgcolor: (theme) =>
              alpha(
                theme.palette.primary.main,
                theme.palette.action.activatedOpacity
              ),
          }),
        }}
      >
        {numSelected > 0 ? (
          <Typography
            sx={{ flex: "1 1 100%", fontSize: 24 }}
            color="inherit"
            variant="subtitle1"
            component="div"
          >
            {numSelected} selected
          </Typography>
        ) : (
          <Typography
            sx={{ flex: "1 1 100%", fontSize: 24 }}
            id="tableTitle"
            component="div"
          >
            Tag Status
          </Typography>
        )}
        {numSelected > 0 ? (
          <Box
            sx={{
              border: 0,
              flexGrow: 1,
              display: "flex",
              justifyContent: "flex-start",
            }}
          >
            <Button
              color="primary"
              variant="contained"
              sx={{ borderRadius: 2, mr: 2, ml: 2, width: 150 }}
              onClick={() => UpdateMultag(selected)}
            >
              <ModeEditIcon sx={{ mr: 1 }} />
              Edit
            </Button>
            <Button
              color="error"
              variant="contained"
              size="small"
              sx={{ borderRadius: 2, width: 150 }}
              onClick={deleteTagsbyids}
            >
              <DeleteForeverRoundedIcon sx={{ mr: 1 }} />
              DELETE
            </Button>
          </Box>
        ) : (
          <Box></Box>
        )}
      </Toolbar>
    );
  };

  EnhancedTableToolbar.propTypes = {
    numSelected: PropTypes.number.isRequired,
  };

  const x = tag.length;
  const y = registag.length;

  let subtractItem = x - y;

  return (
    <ThemeProvider theme={mdTheme}>
      <CssBaseline />
      <Grid container spacing={18}>
        <Grid item xs={12}>
          <Box
            sx={{
              bgcolor: "primary.info",
              width: "auto",
              height: "auto",
              minHeight: 900,
              borderLeft: 71,
              borderColor: "primary.main",
            }}
          >
            <Appbar />
            <Grid container sx={{ pt: 5, mt: 5, mb: 4 }}>
              <Grid item xs={12} sx={{ height: "auto", width: "auto" }}>
                <Box
                  direction="row"
                  display="flex"
                  sx={{ border: 0, pt: 5, pr: 15 }}
                >
                  <Box sx={{ display: "flex", flexGrow: 1 }}>
                    <Card
                      sx={{
                        width: 350,
                        height: 120,
                        m: 3,
                        ml: 15,
                      }}
                      style={{
                        borderRadius: 20,
                      }}
                    >
                      <CardContent sx={{ border: 0, height: 10 }}>
                        <Typography
                          gutterBottom
                          variant="h1"
                          display="flex"
                          style={{ fontSize: 24 }}
                        >
                          Total tags in stock :
                        </Typography>
                      </CardContent>
                      <CardContent
                        sx={{ border: 0, height: 100, mt: 1 }}
                        style={{
                          borderRadius: 10,
                        }}
                      >
                        <Typography
                          gutterBottom
                          variant="h5"
                          display="flex"
                          justifyContent="center"
                          style={{ fontSize: 36 }}
                        >
                          {tag.length}
                        </Typography>
                      </CardContent>
                    </Card>
                    <Card
                      align="center"
                      sx={{ width: 350, height: 120, m: 3 }}
                      style={{
                        borderRadius: 20,
                      }}
                    >
                      <CardContent sx={{ border: 0, height: 10 }}>
                        <Typography
                          gutterBottom
                          variant="h5"
                          display="flex"
                          style={{ fontSize: 24 }}
                        >
                          Total tags registered :
                        </Typography>
                      </CardContent>
                      <CardContent
                        display="flex"
                        sx={{ border: 0, height: 10, mt: 1 }}
                        style={{
                          borderRadius: 10,
                        }}
                      >
                        <Typography
                          gutterBottom
                          variant="h5"
                          display="flex"
                          justifyContent="center"
                          style={{ fontSize: 36 }}
                        >
                          {registag.length}
                        </Typography>
                      </CardContent>
                    </Card>
                    <Card
                      align="center"
                      sx={{ width: 350, height: 120, m: 3 }}
                      style={{
                        borderRadius: 20,
                      }}
                    >
                      <CardContent sx={{ border: 0, height: 10 }}>
                        <Typography
                          gutterBottom
                          variant="h5"
                          display="flex"
                          style={{ fontSize: 24 }}
                        >
                          Total tags unregistered :
                        </Typography>
                      </CardContent>
                      <CardContent
                        display="flex"
                        sx={{ border: 0, height: 10, mt: 1 }}
                        style={{
                          borderRadius: 10,
                        }}
                      >
                        <Typography
                          gutterBottom
                          variant="h5"
                          display="flex"
                          justifyContent="center"
                          style={{ fontSize: 36 }}
                        >
                          {subtractItem}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Box>
                  <Box
                    sx={{
                      border: 0,
                      display: "flex",
                      alignItems: "center",
                    }}
                  >
                    <Stack direction="column">
                      <Box
                        component={Paper}
                        sx={{
                          ml: 0,
                          border: 0,
                          display: "flex",
                          justifyContent: "center",
                          alignItems: "center",
                          width: 300,
                          height: 60,
                        }}
                      >
                        <input
                          type="file"
                          accept={".csv"}
                          onChange={saveFile}
                        />
                      </Box>
                      <Button
                        variant="contained"
                        sx={{ width: 300 }}
                        onClick={addCSV}
                      >
                        upload
                      </Button>
                    </Stack>
                  </Box>
                </Box>

                <Box>
                  <Box
                    sx={{
                      border: 0,
                      mr: 15,
                      pl: 15,
                      mb: 2,
                      display: "flex",
                      justifyContent: "flex-start",
                    }}
                  >
                    <TextField
                      variant="standard"
                      value={searchText}
                      onChange={(e) => {
                        setSearchText(e.target.value);
                        requestSearch(e.target.value);
                      }}
                      placeholder="Search..."
                      InputProps={{
                        startAdornment: (
                          <SearchIcon fontSize="medium" color="action" />
                        ),
                        endAdornment: (
                          <IconButton
                            title="Clear"
                            aria-label="Clear"
                            size="small"
                            style={{
                              visibility: searchText ? "visible" : "hidden",
                              borderRadius: "57%",
                              paddingRight: "1px",
                              margin: "0",
                              fontSize: "1.25rem",
                            }}
                            onClick={(e) => {
                              setSearchText("");
                              setTag(platform);
                            }}
                          >
                            <ClearIcon fontSize="medium" color="action" />
                          </IconButton>
                        ),
                      }}
                      sx={{
                        width: { xs: 1, sm: "auto", flexGrow: 1 },
                        m: (theme) => theme.spacing(1, 0.5, 1.5),
                        "& .MuiSvgIcon-root": {
                          mr: 0.5,
                        },
                        "& .MuiInput-underline:before": {
                          borderBottom: 1,
                          borderColor: "divider",
                        },
                      }}
                    />
                  </Box>
                </Box>
                <Box
                  display="flex"
                  sx={{
                    justifyContent: "center",
                    pb: 15,
                  }}
                >
                  <TableContainer component={Paper} sx={{ mx: 15 }}>
                    <EnhancedTableToolbar numSelected={selected.length} />
                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                      <EnhancedTableHead
                        numSelected={selected.length}
                        onSelectAllClick={handleSelectAllClick}
                        rowCount={tag.length}
                      />

                      <TableBody>
                        {(rowsPerPage > 0
                          ? tag.slice(
                              page * rowsPerPage,
                              page * rowsPerPage + rowsPerPage
                            )
                          : tag
                        ).map((tag, index) => {
                          const isItemSelected = isSelected(tag.tag_id);
                          const labelId = `enhanced-table-checkbox-${index}`;
                          return (
                            <StyledTableRow
                              key={tag.tag_id}
                              value={tag.tag_id}
                              hover
                              onClick={(event) =>
                                handleClick(event, tag.tag_id)
                              }
                              role="checkbox"
                              aria-checked={isItemSelected}
                              selected={isItemSelected}
                            >
                              <StyledTableCell align="center">
                                <Checkbox
                                  value={tag.tag_id}
                                  color="primary"
                                  checked={isItemSelected}
                                  inputProps={{
                                    "aria-labelledby": labelId,
                                  }}
                                />
                              </StyledTableCell>
                              <StyledTableCell
                                component="th"
                                scope="row"
                                align="center"
                              >
                                {tag.tag_id}
                              </StyledTableCell>
                              <StyledTableCell align="left">
                                {tag.tag_detail}
                              </StyledTableCell>
                              <StyledTableCell align="center">
                                {tag.product_name}
                              </StyledTableCell>
                              <StyledTableCell align="center">
                                <Grid
                                  display="flex"
                                  justifyContent="center"
                                  sx={{ width: 300, height: 150, border: 0 }}
                                >
                                  <Avatar
                                    src={tag.product_picture}
                                    sx={{ width: 150, height: 150 }}
                                  />
                                </Grid>
                              </StyledTableCell>
                              <StyledTableCell align="center">
                                {tag.tag_timestamp}
                              </StyledTableCell>
                              <StyledTableCell align="center">
                                <Button
                                  color="primary"
                                  variant="contained"
                                  sx={{
                                    borderRadius: 2,
                                    mb: 0.5,
                                    width: 120,
                                  }}
                                  onClick={() => UpdateTag(tag.tag_id)}
                                >
                                  <ModeEditIcon sx={{ mr: 1 }} />
                                  Edit
                                </Button>
                                <Button
                                  color="error"
                                  variant="contained"
                                  size="small"
                                  onClick={() => TagDelete(tag.tag_id)}
                                  sx={{
                                    borderRadius: 2,
                                    width: 120,
                                  }}
                                >
                                  <DeleteForeverRoundedIcon sx={{ mr: 1 }} />
                                  delete
                                </Button>
                              </StyledTableCell>
                            </StyledTableRow>
                          );
                        })}
                      </TableBody>
                      <TableFooter>
                        <TableRow sx={{ borderTop: 0 }}>
                          <TablePagination
                            rowsPerPageOptions={[
                              10,
                              25,
                              50,
                              100,
                              { label: "All", value: -1 },
                            ]}
                            colSpan={5}
                            count={tag.length}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            SelectProps={{
                              inputProps: {
                                "aria-label": "rows per page",
                              },
                              native: true,
                            }}
                            onPageChange={handleChangePage}
                            onRowsPerPageChange={handleChangeRowsPerPage}
                            ActionsComponent={TablePaginationActions}
                          />
                        </TableRow>
                      </TableFooter>
                    </Table>
                  </TableContainer>
                </Box>
              </Grid>
            </Grid>
          </Box>
        </Grid>
      </Grid>
    </ThemeProvider>
  );
}
