import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

// MUI import
import CssBaseline from "@mui/material/CssBaseline";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Avatar from "@mui/material/Avatar";
import Typography from "@mui/material/Typography";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import Card from "@mui/material/Card";
import Stack from "@mui/material/Stack";
import TextField from "@mui/material/TextField";

import Swal from "sweetalert2";

//import component
import Appbar from "./appbar";

const theme = createTheme({
  palette: {
    primary: {
      main: "#142454",
    },
  },
});

export default function Profile() {
  const { id } = useParams();
  useEffect(() => {
    fetch("https://crud-wms.herokuapp.com/users/" + id)
      .then((res) => res.json())
      .then((result) => {
        setFname(result.fname);
        setLname(result.lname);
        setAvatar(result.avatar);
        setContact(result.contact);
      });
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();
    var data = {
      user_id: id,
      fname: fname,
      lname: lname,
      avatar: avatar,
      contact: contact,
    };
    fetch("https://crud-wms.herokuapp.com/updateProfile", {
      method: "PUT",
      headers: {
        Accept: "application/form-data",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((result) => {
        const ok = JSON.stringify(result.status);
        if (ok == ok) {
          Swal.fire({
            icon: "success",
            title: "Updated!",
            text: result["message"],
            timer: 2000,
            timerProgressBar: true,
            showConfirmButton: false,
          });
          window.setTimeout(function () {
            window.location.href = "/Profile";
          }, 2000);
        }
      })

      .catch((error) => console.log("error", error));
  };

  const [fname, setFname] = useState("");
  const [lname, setLname] = useState("");
  const [avatar, setAvatar] = useState("");
  const [contact, setContact] = useState("");

  return (
    <ThemeProvider theme={theme}>
      <Appbar />
      <Grid container component="main" sx={{ height: "100vh" }}>
        <CssBaseline />
        <Grid
          item
          xs={false}
          sm={4}
          md={7}
          sx={{
            backgroundImage:
              "url(https://cdn.discordapp.com/attachments/744696824220221502/984030671427231775/unknown.png)",
            backgroundRepeat: "no-repeat",
            backgroundColor: (t) =>
              t.palette.mode === "light"
                ? t.palette.grey[50]
                : t.palette.grey[900],
            backgroundSize: "cover",
            backgroundPosition: "center",
          }}
        />

        <Grid
          item
          justifyContent="center"
          xs={12}
          sm={8}
          md={5}
          component={Paper}
          sx={{ pt: 18, backgroundColor: "#e7e8fc" }}
        >
          <form onSubmit={handleSubmit}>
            <Grid
              item
              sx={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Avatar
                alt="prof_pic"
                src={avatar}
                sx={{
                  border: 15,
                  borderColor: "primary.main",
                  width: theme.spacing(40),
                  height: theme.spacing(40),
                  boxShadow: theme.shadows[3],
                  mb: 3,
                }}
              />
              <Card raised="true" sx={{ width: 600, borderRadius: 5, p: 3 }}>
                <Typography
                  sx={{
                    fontSize: 36,
                    mb: 2,
                    color: "primary.main",
                    flexGrow: 1,
                  }}
                >
                  My Profile
                </Typography>
                <Grid container justifyContent="center" sx={{ mb: 2 }}>
                  <Stack direction="row">
                    <Grid
                      container
                      justifyContent="flex-start"
                      alignItems="center"
                      xs={12}
                      sm={6}
                      sx={{ border: 0 }}
                    >
                      <TextField
                        justifyContent="flex-start"
                        autoComplete="fname"
                        name="firstName"
                        variant="standard"
                        required
                        id="firstName"
                        label="First Name"
                        value={fname}
                        sx={{ width: 237 }}
                        onChange={(e) => setFname(e.target.value)}
                        autoFocus
                      />
                    </Grid>
                    <Grid
                      container
                      justifyContent="flex-start"
                      xs={12}
                      sx={{ pl: 1 }}
                    >
                      <TextField
                        variant="standard"
                        required
                        id="lastName"
                        label="Last Name"
                        value={lname}
                        sx={{ width: 243 }}
                        onChange={(e) => setLname(e.target.value)}
                      />
                    </Grid>
                  </Stack>
                </Grid>
                <Grid container justifyContent="center" xs={12} sx={{ mb: 2 }}>
                  <TextField
                    variant="standard"
                    required
                    id="avatar"
                    label="User avatar"
                    value={avatar}
                    sx={{ width: 490 }}
                    onChange={(e) => setAvatar(e.target.value)}
                  />
                </Grid>
                <Grid container justifyContent="center" xs={12} sx={{ mb: 2 }}>
                  <TextField
                    variant="standard"
                    required
                    id="contact"
                    label="Contact number"
                    value={contact}
                    sx={{ width: 490, mb: 2 }}
                    onChange={(e) => setContact(e.target.value)}
                  />
                  <Button
                    variant="contained"
                    type="submit"
                    sx={{
                      width: 100,
                      height: 40,
                      borderRadius: 10,
                      mt: 0.75,
                      mr: 2,
                    }}
                  >
                    Submit
                  </Button>
                </Grid>
              </Card>
            </Grid>
          </form>
        </Grid>
      </Grid>
    </ThemeProvider>
  );
}
