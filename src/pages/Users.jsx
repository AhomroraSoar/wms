import React, { useEffect, useState } from "react";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Avatar from "@mui/material/Avatar";
import { Link } from "react-router-dom";
import { createTheme, useTheme, ThemeProvider } from "@mui/material/styles";
import Grid from "@mui/material/Grid";
import { styled } from "@mui/material/styles";
import PropTypes from "prop-types";
import TextField from "@mui/material/TextField";
import IconButton from "@mui/material/IconButton";
import TableFooter from "@mui/material/TableFooter";
import TablePagination from "@mui/material/TablePagination";

import Appbar from "./appbar";

import Swal from "sweetalert2";

//import icons
import DeleteForeverRoundedIcon from "@mui/icons-material/DeleteForeverRounded";
import AddIcon from "@mui/icons-material/Add";
import ClearIcon from "@mui/icons-material/Clear";
import SearchIcon from "@mui/icons-material/Search";
import FirstPageIcon from "@mui/icons-material/FirstPage";
import KeyboardArrowLeft from "@mui/icons-material/KeyboardArrowLeft";
import KeyboardArrowRight from "@mui/icons-material/KeyboardArrowRight";
import LastPageIcon from "@mui/icons-material/LastPage";

function TablePaginationActions(props) {
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (event) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box sx={{ flexShrink: 0, ml: 2.5, border: 0 }}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowRight />
        ) : (
          <KeyboardArrowLeft />
        )}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowLeft />
        ) : (
          <KeyboardArrowRight />
        )}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </Box>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};

export default function Users() {
  const [platform, setPlatform] = useState([]);
  const [searchText, setSearchText] = useState("");
  const [rows] = useState([]);
  const [users, setUsers] = useState([]);
  useEffect(() => {
    UsersGet();
  }, []);

  const UsersGet = () => {
    fetch("https://crud-wms.herokuapp.com/users")
      .then((res) => res.json())
      .then((rows) => {
        setUsers(rows);
        setPlatform(rows);
      });
  };

  function escapeRegExp(value) {
    return value.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
  }

  const requestSearch = (searchValue) => {
    const searchRegex = new RegExp(escapeRegExp(searchValue), "i");
    const filteredUsers = platform.filter((users) => {
      return Object.keys(users).some((field) => {
        return searchRegex.test(users[field]);
      });
    });
    setUsers(filteredUsers);
  };

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const UpdateUser = (user_id) => {
    window.location = "/Update/" + user_id;
  };

  const UserDelete = (user_id) => {
    var data = {
      user_id: user_id,
    };
    fetch("https://crud-wms.herokuapp.com/delete", {
      method: "DELETE",
      headers: {
        Accept: "application/form-data",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then((rows) => {
        if (rows["status"] === "ok") {
          Swal.fire({
            icon: "success",
            title: "Deleted!",
            text: rows["message"],
            toast: true,
            position: "top-end",
            showConfirmButton: false,
            timer: 2500,
            timerProgressBar: true,
          });
          UsersGet();
          window.location = "/Users";
        }
      });
  };

  const mdTheme = createTheme({
    palette: {
      primary: {
        main: "#001345",
        info: "#e7e8fc",
      },
      secondary: {
        main: "#878787",
        info: "#ffffff",
      },
      neutral: {
        main: "#ffffff",
        contrastText: "#fff",
      },
    },
  });

  const role = JSON.parse(localStorage.getItem("user"));

  const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.primary.main,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 18,
    },
  }));

  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    "&:last-child td, &:last-child th": {
      border: 0,
    },
  }));

  return (
    <ThemeProvider theme={mdTheme}>
      <Grid container spacing={1}>
        <Box
          sx={{
            border: 0,
            bgcolor: "primary.info",
            width: "100%",
            height: "auto",
            minHeight: 890,
            borderLeft: 71,
            borderColor: "primary.main",
          }}
        >
          <Grid item xs={12}>
            <Appbar />
            <Grid>
              <Paper sx={{ p: 3, m: 5, mt: 15 }}>
                <Box display="flex">
                  <Box flexGrow={1}>
                    <Typography
                      component="h2"
                      variant="h6"
                      color="primary.main"
                      gutterBottom
                      sx={{ ml: 3, mt: 1 }}
                    >
                      USERS
                    </Typography>
                  </Box>

                  <Box
                    display="flex"
                    sx={{ justifyContent: "flex-end", mr: 5 }}
                  >
                    <TextField
                      variant="standard"
                      value={searchText}
                      onChange={(e) => {
                        setSearchText(e.target.value);
                        requestSearch(e.target.value);
                      }}
                      placeholder="Search..."
                      InputProps={{
                        startAdornment: (
                          <SearchIcon fontSize="medium" color="action" />
                        ),
                        endAdornment: (
                          <IconButton
                            title="Clear"
                            aria-label="Clear"
                            size="small"
                            style={{
                              visibility: searchText ? "visible" : "hidden",
                              borderRadius: "57%",
                              paddingRight: "1px",
                              margin: "0",
                              fontSize: "1.25rem",
                            }}
                            onClick={(e) => {
                              setSearchText("");
                              setUsers(platform);
                            }}
                          >
                            <ClearIcon fontSize="medium" color="action" />
                          </IconButton>
                        ),
                      }}
                      sx={{
                        width: { xs: 1, sm: "auto" },
                        m: (theme) => theme.spacing(1, 0.5, 1.5),
                        "& .MuiSvgIcon-root": {
                          mr: 0.5,
                        },
                        "& .MuiInput-underline:before": {
                          borderBottom: 1,
                          borderColor: "divider",
                        },
                      }}
                    />
                  </Box>
                  <Box>
                    <Link to="/Register">
                      <Button
                        variant="contained"
                        color="primary"
                        sx={{ width: 150, height: 50, mr: 5 }}
                      >
                        <AddIcon sx={{ mr: 1 }} />
                        CREATE
                      </Button>
                    </Link>
                  </Box>
                </Box>
                <TableContainer component={Paper} sx={{ mt: 2 }}>
                  <Table aria-label="customized table">
                    <TableHead>
                      <StyledTableRow>
                        <StyledTableCell align="center" width="10%">
                          ID
                        </StyledTableCell>
                        <StyledTableCell align="center" width="10%">
                          Avatar
                        </StyledTableCell>
                        <StyledTableCell align="center" width="15%">
                          First name
                        </StyledTableCell>
                        <StyledTableCell align="center" width="15%">
                          Last name
                        </StyledTableCell>
                        <StyledTableCell align="center" width="10%">
                          Email
                        </StyledTableCell>
                        <StyledTableCell align="center" width="10%">
                          Contact
                        </StyledTableCell>
                        <StyledTableCell align="center" width="10%">
                          Role
                        </StyledTableCell>
                        <StyledTableCell align="center" width="10%">
                          Action
                        </StyledTableCell>
                      </StyledTableRow>
                    </TableHead>
                    <TableBody>
                      {(rowsPerPage > 0
                        ? users.slice(
                            page * rowsPerPage,
                            page * rowsPerPage + rowsPerPage
                          )
                        : users
                      ).map((user) => (
                        <StyledTableRow key={user.user_id}>
                          <StyledTableCell align="center">
                            {user.user_id}
                          </StyledTableCell>
                          <StyledTableCell align="center">
                            <Box display="flex" justifyContent="center">
                              <Avatar
                                src={user.avatar}
                                sx={{ width: 120, height: 120 }}
                              />
                            </Box>
                          </StyledTableCell>
                          <StyledTableCell align="center">
                            {user.fname}
                          </StyledTableCell>
                          <StyledTableCell align="center">
                            {user.lname}
                          </StyledTableCell>
                          <StyledTableCell align="center">
                            {user.email}
                          </StyledTableCell>
                          <StyledTableCell align="center">
                            {user.contact}
                          </StyledTableCell>
                          <StyledTableCell align="center">
                            {user.role_name}
                          </StyledTableCell>
                          <StyledTableCell align="center">
                            <Button
                              color="success"
                              variant="contained"
                              onClick={() => UpdateUser(user.user_id)}
                              sx={{ borderRadius: 2 }}
                            >
                              Edit
                            </Button>
                            <Button
                              color="error"
                              size="small"
                              onClick={() => UserDelete(user.user_id)}
                            >
                              <DeleteForeverRoundedIcon fontSize="large" />
                            </Button>
                          </StyledTableCell>
                        </StyledTableRow>
                      ))}
                    </TableBody>
                    <TableFooter>
                      <TableRow sx={{ borderTop: 0 }}>
                        <TablePagination
                          rowsPerPageOptions={[
                            10,
                            25,
                            50,
                            100,
                            { label: "All", value: -1 },
                          ]}
                          colSpan={4}
                          count={users.length}
                          rowsPerPage={rowsPerPage}
                          page={page}
                          SelectProps={{
                            inputProps: {
                              "aria-label": "rows per page",
                            },
                            native: true,
                          }}
                          onPageChange={handleChangePage}
                          onRowsPerPageChange={handleChangeRowsPerPage}
                          ActionsComponent={TablePaginationActions}
                        />
                      </TableRow>
                    </TableFooter>
                  </Table>
                </TableContainer>
              </Paper>
            </Grid>
          </Grid>
        </Box>
      </Grid>
    </ThemeProvider>
  );
}
