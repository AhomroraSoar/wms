import Login from "./pages/Login";
import Profile from "./pages/Profile";
import Users from "./pages/Users";
import Register from "./pages/UserCreate";
import UserUpdate from "./pages/UserUpdate";
import Home from "./pages/Database page/Home";
import Dashboard from "./pages/Dashboard page/Dashboard";
import Category from "./pages/Database page/Category";
import Tag from "./pages/Database page/Tag";
import Missing from "./pages/Missing";
import ProductCreate from "./pages/ProductCreate";
import Product from "./pages/Products";
import ProductUpdate from "./pages/ProductUpdate";
import CategoryCreate from "./pages/Database page/CategoryCreate";
import CategoryUpdate from "./pages/Database page/CategoryUpdate";
import TagUpdate from "./pages/Database page/TagUpdate";
import MultagsUpdate from "./pages/Database page/MultagsUpdate";
import Categorizedpd from "./pages/Database page/Catagorizedpd";
import UpdateProfile from "./pages/ProfileEdit";

import { Routes, Route } from "react-router-dom";

function App() {
  return (
    <Routes>
      {/* public routes */}
      <Route path="/" element={<Login />} />
      <Route path="Register" element={<Register />} />

      <Route path="Users" element={<Users />} />
      <Route path="Profile" element={<Profile />} />
      <Route path="Update/:id" element={<UserUpdate />} />
      <Route path="Updateprofile/:id" element={<UpdateProfile />} />

      <Route path="Products" element={<Product />} />
      <Route path="ProductRegister" element={<ProductCreate />} />
      <Route path="Updatepd/:id" element={<ProductUpdate />} />

      <Route path="Home" element={<Home />} />
      <Route path="Dashboard" element={<Dashboard />} />

      <Route path="Category" element={<Category />} />
      <Route path="CategoryCreate" element={<CategoryCreate />} />
      <Route path="Category/:id" element={<Categorizedpd />} />
      <Route path="UpdateCategory/:id" element={<CategoryUpdate />} />

      <Route path="Tag" element={<Tag />} />
      <Route path="UpdateTag/:id" element={<TagUpdate />} />
      <Route path="UpdatemultipleTags/:id" element={<MultagsUpdate />} />

      {/* catch all */}
      <Route path="*" element={<Missing />} />
    </Routes>
  );
}

export default App;
